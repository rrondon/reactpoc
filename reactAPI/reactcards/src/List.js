import React, { Component } from 'react';

const List = (props) => {
  const{usernames} = props

  return(
    <div className="cardDiv">
      {usernames.map(user =>
        <div className="Card">
          <a href="https://placeholder.com"><img src="https://via.placeholder.com/290"></img></a>
          <div key={user.id}>{user.name}</div>
        </div>
      )}
    </div>
  )
}

export default List
