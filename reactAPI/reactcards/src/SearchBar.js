import React, { Component } from 'react';
import axios from 'axios';

class SearchBar extends Component {
  constructor(props) {
      super(props)

      this.state = {
        users: [],
        store: []
      }
    }

    componentDidMount() {
      axios.get('https://randomuser.me/api/?results=10&inc=name,registered&nat=fr')
      .then(json => json.data.results.map(result => (
        {
          name: `${result.name.first} ${result.name.last}`,
          id: result.registered
        })))
        .then(newData => this.setState({users: newData, store: newData}))
        .catch(error => alert(error))
    }

    filterNames(e) {
      this.setState({users: this.state.store.filter(item => item.name.toLowerCase().includes(e.target.value.toLowerCase()))})
    }


    render() {
      const {users} = this.state

      return(
        <div className="searchBarDiv">
          <h1 className="searchBarTitle">Keyword</h1>
          <input className="searchBar" type="text" placeholder="search" onChange={props.searchFunction} />
        </div>
      );
    }
  }

export default SearchBar
