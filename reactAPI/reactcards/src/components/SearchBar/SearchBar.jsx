import React, { Component } from 'react';
import axios from 'axios';

class SearchBar extends Component {
  constructor(props) {
      super(props)

      this.state = {
        users: [],
      }
    }

    componentDidMount() {
      axios.get('https://randomuser.me/api/?results=10&inc=name,registered&nat=fr')
      .then(json => json.data.results.map(result => (
        {
          name: `${result.name.first} ${result.name.last}`,
          id: result.registered
        })))
        .then(newData => this.setState({users: newData}))
        .catch(error => alert(error))
    }

    handleInputChange = () => {
      this.setState({
        users: this.search.value
      })
    }

    render() {
      const {users} = this.state

      return(
        <div className="searchBarDiv">
          <h1 className="searchBarTitle">Keyword</h1>
          <form>
            <input
              className="searchBar"
              type="text"
              placeholder="search"
              ref={input => this.search = input}
              onChange={this.handleInputChange}
            />
          </form>
        </div>
      );
    }
  }

export default SearchBar
