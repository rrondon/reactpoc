import React, { Component } from 'react';
import './sliderpanel.css';

const SliderPanel = (props) => {
    const { description } = props;

    return(
      <aside className="panel">
        <h2 className="panelHeader">{props.description}</h2>
      </aside>
    )
  };
  
export default SliderPanel;
