import React from 'react';
import PageTwo from './PageTwo/pagetwo';

const PageTwoLayout = () => (
  <div>
    <PageTwo />
  </div>
)

export default PageTwoLayout
