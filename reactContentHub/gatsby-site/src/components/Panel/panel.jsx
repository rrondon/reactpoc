import React, { Component } from 'react';
import './panel.css';
import Article from '../Article/article';


class Sidebar extends Component {
  constructor(props) {
    super(props)

    this.state = {

    }
  }
  
  render() {
    return(
      <div className="panel">
        <p className="panelTitle">Editors Picks</p>
        <Article />
        <Article />
        <Article />
      </div>
    );
  }
}

export default Sidebar
