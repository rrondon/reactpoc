import React from 'react';
import Interior from './Interior/interior';
import './layout.css';

const PageLayout = () => (
      <div>
        <Interior />
      </div>
    )
export default PageLayout
