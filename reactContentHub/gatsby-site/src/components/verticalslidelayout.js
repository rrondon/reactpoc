import React from 'react';
import VerticalSlider from './VerticalSlider/verticalslider';

const VerticalSlideLayout = () => (
  <div>
    <VerticalSlider />
  </div>
)

export default VerticalSlideLayout;
