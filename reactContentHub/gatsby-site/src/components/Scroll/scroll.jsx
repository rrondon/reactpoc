import React, { Component } from 'react';
import './scroll.css';

class Scroll extends Component {

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll(event) {
    let winScroll = document.body.scrollTop || document.documentElement.scrollTop;
    let height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    let scrolled = (winScroll / height) * 100;
    document.getElementById('progressBar').style.width = scrolled + "%";
  }

    render() {
      return(
        <div className="scrollContainer">
          <div className="scroll">
            <div className="progressContainer">
              <div className="progressBar" id="progressBar"></div>
            </div>
          </div>
      </div>
      )
    }
};

export default Scroll;
