import React, { Component } from 'react';
import axios from 'axios';
import Scroll from '../Scroll/scroll';
import PageTwoRail from '../PageTwoRail/pagetworail';
import PageTwoList from '../PageTwoList/pagetwolist';
import './pagetwo.css';

class PagesTwo extends Component {
  constructor(props) {
    super(props)

    this.state = {
      count: 0,
      img: '',
      data: [],
      id: '',
      description: '',
      isLoading: false,
      likes: 0,
      visible: 5
    }
    this.loadMore = this.loadMore.bind(this);
  }

  loadMore() {
    this.setState((prev) => {
      return {
        visible: prev.visible + 5
      }
    });
  }

  componentDidMount() {
    fetch('https://api.unsplash.com/photos/?client_id=e3d37a3f6d41c25020197820a612d8b7c0ff0c78f99edba1f82a38ee6eea87f1&query=animal&orientation=portrait', {
      method: 'GET',
  })
  .then((results) => {
    results.json().then((data) => {
      this.setState({
        data: data,
        id: data[0].id,
        img: data[0].urls.small,
        description: data[0].description,
        likes: data[0].total_likes,
        isLoading: true
      })
    })
  })
  .catch(err => {
    console.log('Error happened during fetching', err);
  });
}

componentWillUnmount() {
  this.setState({
    isLoading: false
  })
}

  showCards() {
    return this.state.data.slice(0, this.state.visible).map((card) => (
      <PageTwoList
        key={card.id}
        img={card.urls.small}
        description={card.description}
        likes={card.likes}
      />
  ));
  }

render() {
  return(
      <div className="interiorDiv">
            <Scroll />
            <div className="articlePageLayout">
              <div className="cardGrid">
                <div className="cardLayout">
                  {this.showCards()}
                    </div>
                    {this.state.visible < this.state.data.length &&
                        <button onClick={this.loadMore} type="button" className="loadMoreBtn">Load More Posts</button>
                    }
                  </div>
                <section className="rightPanel">
                   <PageTwoRail
                     data={this.state.data}
                     index={this.state.index}
                     description={this.state.description}
                     loadMore={this.loadMore}
                     visible={this.state.visible}
                     likes={this.state.likes}
                  />
                </section>
              </div>
       </div>
    );
  }
}

export default PagesTwo;
