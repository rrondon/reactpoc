import React from 'react';
import Dropdown from './Dropdown/dropdown';

const DropdownLayout = () => (
  <div>
    <Dropdown />
  </div>
)

export default DropdownLayout; 
