import React, { Component } from 'react';
import Selector from '../Selector/Selector';
import './selectors.css';

class Selectors extends Component {
    constructor(props) {
        super(props)

        this.state = {
            count: 0,
            img: '',
            data: [],
            id: '',
            description: '',
            isLoading: false,
            likes: 0,
            visible: 8
        }
    }

    componentDidMount() {
        fetch('https://api.unsplash.com/photos/?client_id=e3d37a3f6d41c25020197820a612d8b7c0ff0c78f99edba1f82a38ee6eea87f1&query=animal&orientation=portrait', {
            method: 'GET',
        })
            .then((results) => {
                results.json().then((data) => {
                    this.setState({
                        data: data,
                        id: data[0].id,
                        img: data[0].urls.small,
                        description: data[0].description,
                        likes: data[0].total_likes,
                        isLoading: true
                    })
                })
            })
            .catch(err => {
                console.log('Error happened during fetching!', err);
            });
    }

    componentWillUnmount() {
        this.setState({
            isLoading: false
        })
    }

    handleClick = (e) => {
        if (this.props.id !== this.props.activeId) {
            this.props.changeSlide(this.props.id);
        } else {
            return;
        }
    }

    render() {

        console.log('this is data in selectors');
        console.log(this.props.data);
        
        return (
            <div className="selectors">
                {this.state.data.map((slide) =>
                    <Selector
                        key={slide.id}
                        id={slide.id}
                        handleClick={this.handleClick}
                        activeId={this.props.activeId}
                        changeSlide={this.props.changeSlide}
                    />
                )}
            </div>
        );
    }
}

export default Selectors;
