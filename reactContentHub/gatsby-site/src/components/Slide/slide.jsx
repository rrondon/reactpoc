import React from 'react';
import './slide.css';

const Slide = (props) => {
    const { src, description, data } = props

  return (
      <div className="slide">
        <img src={src} alt=""></img>
        <div className="slideInfo">
        </div>
        <h1 className="slideDescription">{description}</h1>
      </div>
  )
}

export default Slide;
