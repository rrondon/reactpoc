import React, { Component } from 'react';
import './toggle.css';
import Popular from '../Popular/popular';
import Latest from '../Latest/latest';

class Toggle extends Component {
  constructor(props) {
    super(props)

    this.state = {
      latestClick: true,
      popularClick: false,
      latestClass: 'active',
      popularClass: 'toggleBtn'
    }
  };

  handleLatestClick = () => {
    this.setState({
      latestClick: true,
      popularClick: false,
      latestClass: 'active',
      popularClass: 'toggleBtn'
    })
  };

  handlePopularClick = () => {
    this.setState({
      popularClick: true,
      latestClick: false,
      latestClass: 'toggleBtn',
      popularClass: 'active'
    })
  };

  showPopular = () => {
    if (this.state.popularClick) {
      return (
        <Popular />
      )
    }
  }

  showLatest = () => {
    if (this.state.latestClick) {
      return (
        <Latest />
      )
    }
  }

  render() {
    return(
      <div className="togglePage">
          <div className="popularLatestBtns">
            <button className={this.state.latestClass} onClick={this.handleLatestClick} >
              Latest
            </button>
            <button className={this.state.popularClass} onClick={this.handlePopularClick} >
              Popular
            </button>
          </div>
          <div className="toggleContent">
           {this.showPopular()}
           {this.showLatest()}
        </div>
      </div>
    );
  }
}

export default Toggle;
