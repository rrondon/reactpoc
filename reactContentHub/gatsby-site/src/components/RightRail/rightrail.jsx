import React, { Component } from 'react';
import ArticleNav from '../ArticleNav/ArticleNav';
import './rightrail.css';

class RightRail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            count: 0,
            img: '',
            data: [],
            id: '',
            description: '',
            isLoading: false,
            likes: 0,
            visible: 2
        }

        this.loadMore = this.loadMore.bind(this);
     }

    loadMore() {
        this.setState((prev) => {
            return {
                visible: prev.visible + 2
            }
        });
    }

    componentDidMount() {
        fetch('https://api.unsplash.com/photos/?client_id=e3d37a3f6d41c25020197820a612d8b7c0ff0c78f99edba1f82a38ee6eea87f1', {
            method: 'GET',
        })
            .then((results) => {
                results.json().then((data) => {
                    this.setState({
                        data: data,
                        id: data[0].id,
                        img: data[0].urls.small,
                        description: data[0].description,
                        likes: data[0].total_likes,
                        isLoading: true
                    })
                })
            })
            .catch(err => {
                console.log('Error happened during fetching!', err);
            });
    }

    componentWillUnmount() {
        this.setState({
            isLoading: false
        })
    }

    showArticleNav() {
        return this.state.data.slice(0, this.state.visible).map((article) => (
            <ArticleNav
                key={article.id}
                img={article.urls.small}
                description={article.description}
                likes={article.likes}
            />
      ));
    }

    render() {
        return (
            <div className="rightRailComponent">
                <div className="rightRailLayout">
                    <div className="rightRailList">
                        {this.showArticleNav()}
                    </div>
                    {this.state.visible < this.state.data.length &&
                        <button onClick={this.loadMore} type="button" className="articleLoadMoreBtn">Load More Posts</button>
                    }
                </div>
            </div>
        );
    }
}


export default RightRail;
