import React, { Component } from 'react';
import Slide from '../Slide/slide';
import Scroll from '../Scroll/scroll';
import Accordion from '../Accordion/accordion';
import RightRail from '../RightRail/rightrail';
import './interior.css';

class Interior extends Component {
  constructor(props) {
    super(props)

    this.state = {
      img: '',
      id: '',
      description: '',
      isLoading: false
    };
  }

  componentDidMount() {
    fetch('https://api.unsplash.com/photos/?client_id=e3d37a3f6d41c25020197820a612d8b7c0ff0c78f99edba1f82a38ee6eea87f1&query=animal&orientation=portrait', {
      method: 'GET',
    })
    .then((results) => {
      results.json().then((data) => {
        this.setState({
          data: data,
          id: data[0].id,
          img: data[0].urls.full,
          description: data[0].description,
          isLoading: true
        })
      })
    })
    .catch(err => {
      console.log('Error happened during fetching!', err);
    });
  }

  componentWillUnmount = () => {
    this.setState({
      isLoading: false
    })
  }

render() {
  return(
    <div className="interiorDiv">
          <Scroll />
          <div className="articlePageLayout">
              <section className="leftPanel">
                <Slide
                  key={this.state.id}
                  src={this.state.img}
                />
                <div className="interiorInfo">
                  <h1>{this.state.description}</h1>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                  <br></br>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                  <br></br>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                  <br></br>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                  <br></br>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                  <br></br>
                </div>
                <Accordion />
             </section>
              <section className="rightPanel">
                 <RightRail />
              </section>
           </div>
    </div>
  )
  }
}

export default Interior;
