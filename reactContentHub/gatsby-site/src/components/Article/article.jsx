import React from 'react';
import './article.css';

const Article = (props) => {
  const { title } = props

  return(
    <div className="articleDiv">
      <div className="articleLayout">
        <p className="articleLink">Article Title</p>
      </div>
    </div>
  )
}

export default Article
