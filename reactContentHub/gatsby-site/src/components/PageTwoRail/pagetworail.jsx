import React, { Component } from 'react';
import PageTwoNav from '../PageTwoNav/pagetwonav';
import './pagetworail.css';

const PageTwoRail = ({ data, index, description, loadMore, visible, likes }) => {

  const theArticles = data.slice(0, visible).map((articleNav, i) => {
    let active = ( i === index ) ? true : false

    return (
      <PageTwoNav
        title="ArticleNav"
        key={i}
        id={i}
        active={active}
        description={articleNav.description}
        loadMore={loadMore}
        likes={articleNav.likes}
      />
    )
  })

  return (
    <div className="articlesDiv">
      { theArticles }
      { visible < data.length &&
        <button onClick={loadMore} type="button" className="loadMoreBtn">Load More</button>
      }
    </div>
  )
}


export default PageTwoRail;
