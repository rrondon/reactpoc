import React, { Component } from 'react';
import './selector.css';

class Selector extends Component {
    constructor(props) {
        super(props)

        this.state = {
            class: 'selector'
        }
    }

    render() {
        if (this.props.activeId === this.props.id) {
            this.setState({
                class: 'activeSelector'
            })
        }

        return (
            <div className={this.state.class}
                onClick={this.props.handleClick.bind(this)}>
            </div>
        );
    }
}

export default Selector;