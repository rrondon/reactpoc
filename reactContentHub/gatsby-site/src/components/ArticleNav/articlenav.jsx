import React from 'react';
import './articlenav.css';

const ArticleNav = ({ description, likes }) => {

    return (
        <div className="articleNavItem">
             <div className="articleItem">
                <div className="articleDescription">
                   <p className="articleInfo">{description}</p>
                   <p className="articleInfo">{likes}</p>
                </div>
              </div>
        </div>
    )
}

export default ArticleNav;
