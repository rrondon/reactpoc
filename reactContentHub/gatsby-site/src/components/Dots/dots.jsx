import React from 'react';
import Dot from '../Dot/dot';
import './dots.css';

const Dots = ({ data, index, dotClick }) => {

  const theDots = data.map((dot, i) => {
    let active = ( i === index ) ? true : false

    return (
      <Dot
        title="Dot"
        key={i}
        id={i}
        active={active}
        dotClick={dotClick}
      />
    )
  })

  return (
    <div className="dotsContainer">
      { theDots }
    </div>
  )
}

export default Dots;
