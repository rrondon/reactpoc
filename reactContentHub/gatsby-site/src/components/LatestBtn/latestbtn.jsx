import React from 'react';
import './latestbtn.css';

const LatestBtn =  ( props ) => {
  return(
    <div className="latestBtn" type="button" onClick={props.showLatest}>
      Latest
    </div>
  )
}

export default LatestBtn;
