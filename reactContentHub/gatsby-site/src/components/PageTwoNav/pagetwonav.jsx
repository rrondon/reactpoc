import React from 'react';
import './pagetwonav.css';

const PageTwoNav = ({ description, likes }) => {

    return (
        <div className="articleNavItem">
             <div className="articleItem">
                <div className="articleDescription">
                   <p className="articleInfo">{description}</p>
                     <p className="articleInfo">Total likes: {likes}</p>
                </div>
              </div>
        </div>
    )
}

export default PageTwoNav;
