import React from 'react';
import './popularbtn.css';

const PopularBtn = ( props ) => {
  return (
    <button onClick={props.handlePopularClick} className="popularBtn" type="button">Popular</button>
  )
}

export default PopularBtn;
